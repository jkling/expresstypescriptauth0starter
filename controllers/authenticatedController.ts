import express from "express";
import jwt from "express-jwt";
import jwks from "jwks-rsa";

import config from "../config";

export const jwtCheck: jwt.RequestHandler = jwt({
    secret: jwks.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: config.auth.jwksUri,
    }),
    audience: config.auth.audience,
    issuer: config.auth.issuer,
    algorithms: ["RS256"],
});

const authenticatedController = express.Router();

authenticatedController.use(jwtCheck);

authenticatedController.get("/authenticated", (req, res) => {
    if (!req.user) {
        res.sendStatus(500);
    }
    res.send(`Authenticated as ${req.user!.sub}\n`);
});

export default authenticatedController;
