import express from "express";

const publicController = express.Router();

publicController.get("/public", (req, res) => {
    res.send("Public route\n");
});

export default publicController;
