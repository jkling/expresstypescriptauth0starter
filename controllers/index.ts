import publicController from "./publicController";
import authenticatedController from "./authenticatedController";

export default [publicController, authenticatedController];
