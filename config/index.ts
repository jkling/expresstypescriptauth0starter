import { auth } from "./auth0";

const configSettings = {
    auth,
};

export default configSettings;
