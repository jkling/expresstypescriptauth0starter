import express from "express";
import controllers from "./controllers";

const app: express.Application = express();
const port: number = 8080;

app.use("/", ...controllers);

app.listen(port, () => {
    console.log(`Listening on port: ${port}`);
});
