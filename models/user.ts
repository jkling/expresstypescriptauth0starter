export type IUser = {
    iss: string;
    sub: string;
    aud: string;
    iat: Date;
    exp: Date;
};
