# Usage

Use degit to make a copy of this repo

## Expected Env Variables

`AUTH_AUDIENCE`, `AUTH_ISSUER`, `AUTH_JWKS_URI`, the values for all of these can be taken from the Auth0 portal

<code>
npx degit https://gitlab.com/jkling/expresstypescriptauth0starter
</code>
